# pixel2svg

`pixel2svg` converts pixel art to SVG – pixel by pixel.

- This script was originally written for Python 2 by Florian Berger: [pixel2svg](https://florian-berger.de/en/software/pixel2svg/).
- This version is based on Cyrille Chopelet's fork: [pixel2svg](https://github.com/cyChop/pixel2svg-fork).

## About

For example, here is an icon from the [Tango Icon Set](http://tango.freedesktop.org/):

![tango heart](http://static.florian-berger.de/tango-heart.png)

If you scale this up for a nice blocky print, you might get a dithered result:

![tango heart 400px dithered](http://static.florian-berger.de/tango-heart-400px-dithered.png)

Of course you can turn dithering off. But sometimes you might want a vector file, especially for large prints. For these cases, pixel2svg produces this SVG file (try clicking to find out whether your browser supports SVG):

[tango-heart.svg](http://static.florian-berger.de/tango-heart.svg)

Here is a screenshot of the SVG in [Inkscape](http://inkscape.org/):

![tango heart inkscape](http://static.florian-berger.de/tango-heart-inkscape.png)

Nice, pure vector data.


## Prerequisites

- Python 3.x   [http://www.python.org](http://www.python.org)
- Python Imaging Library (Pillow) [https://pillow.readthedocs.io/en/stable/](https://pillow.readthedocs.io/en/stable/)
- svgwrite [http://pypi.python.org/pypi/svgwrite/](http://pypi.python.org/pypi/svgwrite/)


## Installation

Install the dependencies with your package manager or with venv / pip.

Then download [`pixel2svg.py`](https://gitlab.com/a.l.e/pixel2svg/-/raw/master/pixel2svg.py?inline=false).

## Usage

    Usage: pixel2svg [--overlap] imagefile

    Options:
      --version             show program's version number and exit
      -h, --help            show this help message and exit
      --squaresize=SQUARESIZE
                            Width and heigt of vector squares in pixels, default: 40
      --overlap             If given, overlap vector squares by 1px

Running

    pixel2svg.py image.ext

will process image.ext and create image.svg.

ext can be any format (png, jpg etc.) that can be read by the Python Imaging Library.

## License

pixel2svg is licensed under the GPL. See the header of `pixel2svg.py` for the details
